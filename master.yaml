AWSTemplateFormatVersion: "2010-09-09"
Description: >
    Master Stack to create a Atlassian Jira
    Initial
    Paul Manalili - manalilipaul@gmail.com - 30/10/2020

Parameters:

  Environment:
    Description: "Jira Environment"
    Default: "DEV"
    Type: "String"
    AllowedValues:
      - "DEV"
      - "PROD"

  DefaultIP:
    Default: "0.0.0.0/0"
    Description: "Update Default IP"
    Type: "String"

  EC2KeyPair:
    Description: "Enter an existing EC2 KeyPair. Default is mk2acc-nv."
    Default: "dev_aws_training"
    Type: AWS::EC2::KeyPair::KeyName

  S3URL:
    Default: "https://atlassian-technical-exercise-paulmanalili.s3-ap-southeast-1.amazonaws.com/infra"
    Description: "Trying running from own directory/workstation."
    Type: "String"

  EC2JiraInstanceType:
    Description: "Choose EC2 Jira Instance Type"
    Type: "String"
    Default: "t2.medium"
    AllowedValues:
      - "t2.medium"
      - "m3.medium"
      - "m4.large"
      - "m4.xlarge"

Mappings:
  RegionMap:
    ap-southeast-1:
      AMI: "ami-015a6758451df3cb9"
      StorageType: "GLACIER"
      CertARN: "arn:aws:acm:ap-southeast-1:290789398543:certificate/dcae57df-ea89-4019-819c-9cada0fee25e"

  EnvironmentMap:
    DEV:
      DBUser: "devadmin"
      DBPassword: "devpassword"
      DBName: "devatlassianjira"
      DBSize: '5'
      DBEngine: "mysql"
      DBEngineVersion: 5.7
      DBInstanceClass: "db.t2.micro"
      VPCCIDR: 10.0.0.0/16
      PublicSubnetCIDR1: 10.0.1.0/24
      PrivateSubnetCIDR1: 10.0.2.0/24
      PublicSubnetCIDR2: 10.0.3.0/24
      PrivateSubnetCIDR2: 10.0.4.0/24
      # DomainRDS: "dev-rds.paulmanalili.site"
      # DomainJira: "dev-jira.paulmanalili.site"
      HostedZone: "paulmanalili.site"
    PROD:
      DBUser: "prodadmin"
      DBPassword: "prodpassword"
      DBName: "prodatlassianjira"
      DBSize: '5'
      DBEngine: "mysql"
      DBEngineVersion: 5.7
      DBInstanceClass: "db.t2.medium"
      VPCCIDR: 10.0.0.0/16
      PublicSubnetCIDR1: 10.0.1.0/24
      PrivateSubnetCIDR1: 10.0.2.0/24
      PublicSubnetCIDR2: 10.0.3.0/24
      PrivateSubnetCIDR2: 10.0.4.0/24
      # DomainRDS: "prod-rds.paulmanalili.site"
      # DomainJira: "prod-jira.paulmanalili.site"
      HostedZone: "paulmanalili.site"

Resources:

  IAM:
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL: !Sub "${S3URL}/iam.yaml"
      Parameters:
        Environment: !Ref "Environment"

  S3:
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL: !Sub "${S3URL}/s3.yaml"
      Parameters:
        Environment: !Ref "Environment"
        StorageType: !FindInMap ["RegionMap", !Ref "AWS::Region", "StorageType"]

  VPC:
    Type: "AWS::CloudFormation::Stack"
    DependsOn:
    - "IAM"
    - "S3"
    Properties:
      TemplateURL: !Sub "${S3URL}/vpc.yaml"
      Parameters:
        Environment: !Ref "Environment"
        VPCCIDR: !FindInMap ["EnvironmentMap", !Ref "Environment", "VPCCIDR"]
        PublicSubnetCIDR1:  !FindInMap ["EnvironmentMap", !Ref "Environment", "PublicSubnetCIDR1"]
        PrivateSubnetCIDR1:  !FindInMap ["EnvironmentMap", !Ref "Environment", "PrivateSubnetCIDR1"]
        PublicSubnetCIDR2:  !FindInMap ["EnvironmentMap", !Ref "Environment", "PublicSubnetCIDR2"]
        PrivateSubnetCIDR2:  !FindInMap ["EnvironmentMap", !Ref "Environment", "PrivateSubnetCIDR2"]
        VPCIAMRoleArn: !GetAtt "IAM.Outputs.VPCIAMRoleArn"

  SecurityGroup:
    Type: "AWS::CloudFormation::Stack"
    DependsOn:
    - "VPC"
    Properties:
      TemplateURL: !Sub "${S3URL}/securitygroup.yaml"
      Parameters:
        Environment: !Ref "Environment"
        DefaultIP: !Ref "DefaultIP"
        VPCCIDR: !FindInMap ["EnvironmentMap", !Ref "Environment", "VPCCIDR"]
        VPC: !GetAtt "VPC.Outputs.VPC"
        PublicNetworkACL: !GetAtt "VPC.Outputs.PublicNetworkACL"
        PrivateNetworkACL: !GetAtt "VPC.Outputs.PrivateNetworkACL"

  RDS:
    Type: "AWS::CloudFormation::Stack"
    DependsOn:
    - "SecurityGroup"
    Properties:
      TemplateURL: !Sub "${S3URL}/rds.yaml"
      Parameters:
        Environment: !Ref "Environment"
        DBUser: !FindInMap ["EnvironmentMap", !Ref "Environment", "DBUser"]
        DBPassword: !FindInMap ["EnvironmentMap", !Ref "Environment", "DBPassword"]
        DBName: !FindInMap ["EnvironmentMap", !Ref "Environment", "DBName"]
        DBSize: !FindInMap ["EnvironmentMap", !Ref "Environment", "DBSize"]
        DBEngine: !FindInMap ["EnvironmentMap", !Ref "Environment", "DBEngine"]
        DBInstanceClass: !FindInMap ["EnvironmentMap", !Ref "Environment", "DBInstanceClass"]
        DBEngineVersion: !FindInMap ["EnvironmentMap", !Ref "Environment", "DBEngineVersion"]
        SecurityGroupJiraRDS: !GetAtt "SecurityGroup.Outputs.SecurityGroupJiraRDS"
        PrivateSubnets: !GetAtt "VPC.Outputs.PrivateSubnets"
        DomainRDS: !FindInMap ["EnvironmentMap", !Ref "Environment", "DomainRDS"]
        HostedZone: !FindInMap ["EnvironmentMap", !Ref "Environment", "HostedZone"]

  ELB:
    Type: "AWS::CloudFormation::Stack"
    DependsOn:
    - "RDS"
    Properties:
      TemplateURL: !Sub "${S3URL}/elb.yaml"
      Parameters:
        Environment: !Ref "Environment"
        CertARN: !FindInMap ["RegionMap", !Ref "AWS::Region", "CertARN"]
        SecurityGroupJiraELB: !GetAtt "SecurityGroup.Outputs.SecurityGroupJiraELB"
        S3Logging: !GetAtt "S3.Outputs.S3Logging"
        PublicSubnets: !GetAtt "VPC.Outputs.PublicSubnets"


  ASGJira:
    Type: "AWS::CloudFormation::Stack"
    DependsOn:
    - "ELB"
    Properties:
      TemplateURL: !Sub "${S3URL}/asg-jira.yaml"
      Parameters:
        Environment: !Ref "Environment"
        EC2KeyPair: !Ref "EC2KeyPair"
        EC2JiraInstanceType: !Ref "EC2JiraInstanceType"
        RegionAMI: !FindInMap ["RegionMap", !Ref "AWS::Region", "AMI"]
        SecurityGroupJiraEC2: !GetAtt "SecurityGroup.Outputs.SecurityGroupJiraEC2"
        ELB: !GetAtt "ELB.Outputs.ELB"
        S3Backup: !GetAtt "S3.Outputs.S3Backup"
        EC2InstanceIAMProfile: !GetAtt "IAM.Outputs.EC2InstanceIAMProfile"
        PrivateSubnets: !GetAtt "VPC.Outputs.PrivateSubnets"
        DomainJira: !FindInMap ["EnvironmentMap", !Ref "Environment", "DomainJira"]
        AutoscaleMin: '1'
        AutoscaleMax: '1'
        AutoscaleDes: '1'

  Route53:
    Type: "AWS::CloudFormation::Stack"
    DependsOn:
    - "ASGJira"
    Properties:
      TemplateURL: !Sub "${S3URL}/route53.yaml"
      Parameters:
        DomainJira: !FindInMap ["EnvironmentMap", !Ref "Environment", "DomainJira"]
        HostedZone: !FindInMap ["EnvironmentMap", !Ref "Environment", "HostedZone"]
        ELBHostID: !GetAtt "ELB.Outputs.ELBHostID"
        ELBDNS: !GetAtt "ELB.Outputs.ELBDNS"


Outputs:

  JiraURL:
    Description: "Jira Server Site"
    Value: !FindInMap ["EnvironmentMap", !Ref "Environment", "DomainJira"]"

  RDSEndpoint:
    Description: "RDS EndPoint "
    Value: !FindInMap ["EnvironmentMap", !Ref "Environment", "DomainRDS"]"

  CertARN:
    Description: "A reference to SSL Certificate ARN of the region"
    Value: !FindInMap ["RegionMap", "ap-southeast-1", "CertARN"]
    Export:
      Name: !Sub "${AWS::StackName}-CertARN"
